import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import org.apache.commons.io.FileUtils

// the path where the project got generated
Path projectPath = Paths.get(request.outputDirectory, request.artifactId)

// the properties available to the archetype
Properties properties = request.properties
String packageName = properties.get("package")
String resourcePackageName = properties.get("resourcePackageName")
String resourceName = properties.get("resourceName")
String generateIT = properties.get("generateIT")

// convert it into a path, e.g. com/inin/pubapi
String packagePath = packageName.replace(".", "/")

// this is a workaround because passing dynamic vars to the FileSet ignore/exclude is not possible
if (generateIT.equalsIgnoreCase("false")) {
    // need to delete IT file when generating the Resource
    String testPackage = "java/" + packagePath + "/it/tests/" + resourcePackageName
    String fileName = projectPath.resolve(testPackage + "/" + resourceName + "IT.java").toString()
    FileUtils.deleteQuietly(new File(fileName))
} else {
    // need to delete generated server/*/generated files when generating the IT test
    String serverPackage = "java/" + packagePath + "/server/"
    String fileNameService = projectPath.resolve(serverPackage + "service/" + resourcePackageName + "/" + resourceName + "Service.java").toString()
    String fileNameServiceBinder = projectPath.resolve(serverPackage + "service/" + resourcePackageName + "/" + resourceName + "ServiceBinder.java").toString()
    String fileNameResource = projectPath.resolve(serverPackage + "resources/" + resourcePackageName + "/" + resourceName + "Resource.java").toString()
    String fileNameListResource = projectPath.resolve(serverPackage + "resources/" + resourcePackageName + "/" + resourceName + "ListResource.java").toString()
    String fileNameDomain = projectPath.resolve(serverPackage + "domain/" + resourcePackageName + "/" + resourceName + ".java").toString()

    List<String> list = new ArrayList<>()
    list.add(fileNameService)
    list.add(fileNameServiceBinder)
    list.add(fileNameResource)
    list.add(fileNameListResource)
    list.add(fileNameDomain)

    for (String file : list) {
        FileUtils.deleteQuietly(new File(file))
    }
}
//delete the generated pom in e.g main/pom.xml
Files.delete projectPath.resolve("pom.xml")